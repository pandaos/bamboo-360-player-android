package com.pandaos.android.bamboo360player;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;

import com.google.ads.interactivemedia.v3.api.AdDisplayContainer;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.ads.interactivemedia.v3.api.AdsRequest;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.player.ContentProgressProvider;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;
import com.google.vr.sdk.widgets.video.VrVideoEventListener;
import com.google.vr.sdk.widgets.video.VrVideoView;
import com.pandaos.bambooplayer.BambooPlayer;

import static android.content.ContentValues.TAG;

/**
 * Created by orenkosto on 4/4/17.
 */

public class Bamboo360Player extends BambooPlayer implements AdErrorEvent.AdErrorListener, AdsLoader.AdsLoadedListener, AdEvent.AdEventListener {

    Context context;

    /**
     * Arbitrary constants and variable to track load status. In this example, this variable should
     * only be accessed on the UI thread. In a real app, this variable would be code that performs
     * some UI actions when the video is fully loaded.
     */
    public static final int LOAD_VIDEO_STATUS_UNKNOWN = 0;
    public static final int LOAD_VIDEO_STATUS_SUCCESS = 1;
    public static final int LOAD_VIDEO_STATUS_ERROR = 2;

    private static final int PROGRESS_BAR_MAX = 100;

    boolean sent25Percent = false;
    boolean sent50Percent = false;
    boolean sent75Percent = false;
    boolean sent100Percent = false;

    private int loadVideoStatus = LOAD_VIDEO_STATUS_UNKNOWN;
    private static boolean playerStarted = false;

    /** Configuration information for the video. **/
    private VrVideoView.Options videoOptions = new VrVideoView.Options();

    private VideoLoaderTask backgroundVideoLoaderTask;

    private VrVideoView vr_video_view;

    private boolean isMuted;
    private static boolean playbackCanceled = false;

    private ImaSdkFactory imaSdkFactory;
    private AdsLoader adsLoader;
    private AdsManager adsManager;
    private static boolean mIsAdDisplayed = false;

    /**
     * By default, the video will start playing as soon as it is loaded. This can be changed by using
     * {@link VrVideoView#pauseVideo()} after loading the video.
     */
    private boolean isPaused = false;

    public Bamboo360Player(Context context) {
        super(context);
        setup360Player(context);
    }

    public Bamboo360Player(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup360Player(context);
    }

    private void setup360Player(Context ctx) {
        this.context = ctx;
        vr_video_view = (VrVideoView) findViewById(R.id.vr_video_view);
        vr_video_view.setEventListener(new ActivityEventListener());
        vr_video_view.setInfoButtonEnabled(false);

        loadVideoStatus = LOAD_VIDEO_STATUS_UNKNOWN;

        if (BambooPlayerInterface.class.isAssignableFrom(this.context.getClass())) {
            setPlayerInterface((BambooPlayerInterface) this.context);
        }
        sent25Percent = false;
        sent50Percent = false;
        sent75Percent = false;
        sent100Percent = false;

        imaSdkFactory = ImaSdkFactory.getInstance();
        adsLoader = imaSdkFactory.createAdsLoader(context);
        adsLoader.addAdErrorListener(this);
        adsLoader.addAdsLoadedListener(this);
    }

    @Override
    public void inflateLayout(Context ctx) {
        LayoutInflater inflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.bamboo_vr_player, this, true);
    }

    public void play360Video(@NonNull String videoUrl) {
        playbackCanceled = false;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                vr_video_view.setVisibility(VISIBLE);
                kaltura_player_root.setVisibility(GONE);
                kaltura_player_controls.setVisibility(GONE);
                videoView.setVisibility(GONE);
            }
        });
        showProgress();
        this.video = Uri.parse(videoUrl);

        videoOptions.inputFormat = VrVideoView.Options.FORMAT_HLS;
        videoOptions.inputType = VrVideoView.Options.TYPE_MONO;

        // Load the bitmap in a background thread to avoid blocking the UI thread. This operation can
        // take 100s of milliseconds.
        if (backgroundVideoLoaderTask != null) {
            // Cancel any task from a previous intent sent to this activity.
            backgroundVideoLoaderTask.cancel(true);
        }
        backgroundVideoLoaderTask = new VideoLoaderTask();
        backgroundVideoLoaderTask.execute(Pair.create(video, videoOptions));
    }

    public void startVideo(int streamConfigIndex) {
        pause();
        showProgress();
        playerStarted = false;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                vr_video_view.setVisibility(VISIBLE);
                kaltura_player_root.setVisibility(GONE);
                kaltura_player_controls.setVisibility(GONE);
                videoView.setVisibility(GONE);
            }
        });
        try {
            if (entry != null) {
                if (streamConfigIndex > -1) {
                    video = Uri.parse(entry.dataUrl);
                } else {
                    video = Uri.parse(getHlsUrl(entry.id));
                }
                play360Video(video.toString());
            } else if (liveEntry != null) {

                String videoUrl = liveEntry.hlsStreamUrl;
                if (streamConfigIndex > -1) {
                    if (liveEntry.liveStreamConfigurations.size() > streamConfigIndex) {
                        videoUrl = liveEntry.liveStreamConfigurations.get(streamConfigIndex).url;
                    }
                }

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    videoUrl = videoUrl.replace("https://", "http://");
                }
                video = Uri.parse(videoUrl);
                play360Video(video.toString());
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    private void togglePause() {
        if (isPaused) {
            resume();
        } else {
            pause();
        }
//        updateStatusText();
    }

    @Override
    public void resume() {
        super.resume();
        if (adsManager != null && mIsAdDisplayed) {
            adsManager.resume();
        } else if (vr_video_view != null && isPaused && !mIsAdDisplayed) {
            vr_video_view.resumeRendering();
            vr_video_view.playVideo();
            isPaused = false;
            playbackCanceled = false;
        }
        if (playerInterface != null && !playerStarted) {
            playerStarted = true;
            playerInterface.onPlayerStart();
            sendGoogleAnalyticsEvent("Play");
        }
    }

    @Override
    public void pause() {
        super.pause();
        if (adsManager != null && mIsAdDisplayed) {
            adsManager.pause();
        }
        if (vr_video_view != null && !isPaused) {
            vr_video_view.pauseRendering();
            vr_video_view.pauseVideo();
            isPaused = true;
            playbackCanceled = true;
        }
    }

    @Override
    public void stop() {
        super.stop();
        pause();
        try {
            if (backgroundVideoLoaderTask != null) {
                backgroundVideoLoaderTask.cancel(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelPlayback() {
        playbackCanceled = true;
    }

    @Override
    public boolean isPlaying() {
        return !isPaused;
    }

    private void setupAds(String adTagUrl) {
        AdDisplayContainer adDisplayContainer = imaSdkFactory.createAdDisplayContainer();
        adDisplayContainer.setAdContainer(this);

        AdsRequest request = imaSdkFactory.createAdsRequest();
        request.setAdTagUrl(adTagUrl);
        request.setAdDisplayContainer(adDisplayContainer);
        request.setContentProgressProvider(new ContentProgressProvider() {
            @Override
            public VideoProgressUpdate getContentProgress() {
                if (mIsAdDisplayed || vr_video_view == null || vr_video_view.getDuration() <= 0) {
                    return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                }
                return new VideoProgressUpdate(vr_video_view.getCurrentPosition(),
                        vr_video_view.getDuration());
            }
        });

        adsLoader.requestAds(request);
    }

    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        mIsAdDisplayed = false;
        resume();
    }

    @Override
    public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
        adsManager = adsManagerLoadedEvent.getAdsManager();
        adsManager.addAdErrorListener(this);
        adsManager.addAdEventListener(this);
        adsManager.init();
    }

    @Override
    public void onAdEvent(AdEvent adEvent) {
        switch (adEvent.getType()) {
            case LOADED:
                adsManager.start();
                break;
            case CONTENT_PAUSE_REQUESTED:
                pause();
                mIsAdDisplayed = true;
                break;
            case CONTENT_RESUME_REQUESTED:
                mIsAdDisplayed = false;
                resume();
                break;
            case ALL_ADS_COMPLETED:
                if (adsManager != null) {
                    adsManager.destroy();
                    adsManager = null;
                }
                break;
            default:
                break;

        }
    }

    /**
     * Listen to the important events from widget.
     */
    private class ActivityEventListener extends VrVideoEventListener {
        /**
         * Called by video widget on the UI thread when it's done loading the video.
         */
        @Override
        public void onLoadSuccess() {
            try {
                hideProgress();
                if (loadVideoStatus != LOAD_VIDEO_STATUS_SUCCESS) {
                    Log.i(TAG, "Successfully loaded video " + vr_video_view.getDuration());
                    loadVideoStatus = LOAD_VIDEO_STATUS_SUCCESS;
//              seekBar.setMax((int) videoWidgetView.getDuration());
//              updateStatusText();
                    if (!playbackCanceled) {
                        String adTagUrl = formattedAdTagUrl();
                        if (adTagUrl != null && adTagUrl.length() > 0) {
                            setupAds(adTagUrl);
                        } else {
                            resume();
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("cannot start player");
            }
        }

        /**
         * Called by video widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
            // An error here is normally due to being unable to decode the video format.
            hideProgress();
            loadVideoStatus = LOAD_VIDEO_STATUS_ERROR;
//            Toast.makeText(
//                    this.context, "Error loading video: " + errorMessage, Toast.LENGTH_LONG)
//                    .show();
            Log.e(TAG, "Error loading video: " + errorMessage);
            if (playerInterface != null) {
                playerInterface.onPlayerError();
            }
        }

        @Override
        public void onClick() {
            togglePause();
//            kaltura_player_controls.showControls(true);
        }

        /**
         * Update the UI every frame.
         */
        @Override
        public void onNewFrame() {
//            updateStatusText();
//            seekBar.setProgress((int) videoWidgetView.getCurrentPosition());
            long position = vr_video_view.getCurrentPosition();
            long duration = vr_video_view.getDuration();
            int progressValue = (int) ((position * PROGRESS_BAR_MAX) / duration);
            if (progressValue >= ((float) PROGRESS_BAR_MAX * 0.25) && !sent25Percent) {
                sent25Percent = true;
                sendGoogleAnalyticsEvent("PlayReached25");
            } else if (progressValue >= ((float) PROGRESS_BAR_MAX * 0.5) && !sent50Percent) {
                sent50Percent = true;
                sendGoogleAnalyticsEvent("PlayReached50");
            } else if (progressValue >= ((float) PROGRESS_BAR_MAX * 0.75) && !sent75Percent) {
                sent75Percent = true;
                sendGoogleAnalyticsEvent("PlayReached75");
            } else if (progressValue >= ((float) PROGRESS_BAR_MAX * 0.9) && !sent100Percent) {
                sent100Percent = true;
                sendGoogleAnalyticsEvent("PlayReached100");
            }
        }

        /**
         * Make the video play in a loop. This method could also be used to move to the next video in
         * a playlist.
         */
        @Override
        public void onCompletion() {
            vr_video_view.seekTo(0);
            if (adsLoader != null) {
                adsLoader.contentComplete();
            }
            if (playerInterface != null) {
                playerInterface.onPlayerComplete();
            }
        }
    }

    /**
     * Helper class to manage threading.
     */
    class VideoLoaderTask extends AsyncTask<Pair<Uri, VrVideoView.Options>, Void, Boolean> {
        @Override
        protected Boolean doInBackground(final Pair<Uri, VrVideoView.Options>... fileInformation) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        vr_video_view.loadVideo(fileInformation[0].first, fileInformation[0].second);
                    } catch (Exception e) {
                        loadVideoStatus = LOAD_VIDEO_STATUS_ERROR;
                        if (playerInterface != null) {
                            playerInterface.onPlayerError();
                        }
                        Log.e(TAG, "Could not open video: " + e);
                    }
                }
            });

            return true;
        }
    }
}
